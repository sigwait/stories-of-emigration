# run `make hh=1` to get a hyphenated variant
out := _out
book := $(out)/stories_of_emigration
lang := uk

ifdef hh
book := $(book).hyphenated
endif

src := META-INF content.opf $(wildcard *.xhtml *.css *.png *.jpg *.svg)

all: $(book).mobi
epub: $(book).epub

$(out)/%.epub: $(src)
	$(lint1)
	$(mkdir)
	@rm -f $@
	zip -X0 -jq $@ mimetype
	zip -rq $@ $^
ifdef hh
	cp /dev/null $(out)/hyphens.css
	zip -jq $@ $(out)/hyphens.css
	rm $(out)/hyphens.css
	epub-hyphen -l $(lang) $@ > $@.tmp
	mv $@.tmp $@
endif

lint: $(book).epub; epubcheck $<

%.mobi: %.epub; -kindlegen $<

mkdir = @mkdir -p $(dir $@)
define lint1
xmllint *.opf *.xhtml > /dev/null
stylelint *.css
endef
.DELETE_ON_ERRORS:
